import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:incodm/routes.dart';
import 'package:incodm/utils/colors.dart';
import 'package:incodm/utils/widgets/widgets.dart';
import 'package:incodm/webservices/models/authentication.dart';
import 'package:incodm/webservices/webservices.dart';

import '../../utils/decorations.dart';
import '../../utils/styles.dart';
import '../../utils/utils.dart';
import '../../utils/widgets/textFields.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  var email = "";
  var password = "";

  bool isLoading = false;

  get padding => null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            height: getScreenSize(context).height,
            color: blackBackoundColor,
            child: Column(
              children: [
                Expanded(
                  flex: 6,
                  child: Container(
                      decoration: boxDecorationBottomRounded(whiteColor, 20),
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Center(
                            child: SingleChildScrollView(
                              physics: ClampingScrollPhysics(),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  //--------------------------- spcae -----------------------------------

                                  // SizedBox(
                                  //     height:
                                  //         getScreenSize(context).height * 0.1),

                                  const SizedBox(height: 20),

                                  //--------------------------- logo -----------------------------------

                                  Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      width:
                                          getScreenSize(context).width * 0.68,
                                      child: Image.asset(
                                        'assets/images/logo.jpg',
                                        fit: BoxFit.fitHeight,
                                      ),
                                    ),
                                  ),
                                  //--------------------------- spcae -----------------------------------

                                  const SizedBox(height: 20),

                                  //  --------------------------- heading ----------------------------------

                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Sign in",
                                      style: boldWhiteText34(blackTextColor),
                                    ),
                                  ),

                                  //--------------------------- space -----------------------------------

                                  const SizedBox(height: 16),

                                  //--------------------------- sign in heading --------------------------

                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      child: Text(
                                        "Sign in to manage your account",
                                        style:
                                            regularWhiteText18(blackTextColor),
                                      ),
                                    ),
                                  ),

                                  //--------------------------- space -----------------------------------

                                  const SizedBox(height: 30),

                                  //--------------------------- Text field email -------------------------

                                  CustomizedTextField(
                                    name: "Email",
                                    hintText: "Enter Your Email",
                                    keyBoardType: TextInputType.emailAddress,
                                    textChanged: (text) {
                                      email = text;
                                    },
                                  ),
                                  //--------------------------- Text field password -----------------------

                                  const SizedBox(height: 20),

                                  CustomizedTextField(
                                    name: "Password",
                                    hintText: "Enter Your Password",
                                    keyBoardType: TextInputType.visiblePassword,
                                    textChanged: (text) {
                                      password = text;
                                    },
                                  ),

                                  //--------------------------- forgot password ----------------------------

                                  const SizedBox(height: 30),
                                  Align(
                                    alignment: Alignment.centerRight,
                                    child: GestureDetector(
                                      onTap: () {
                                        // Navigator.pushNamed(
                                        //     context, Routes.forgotPassword);

                                        // Navigator.pushNamed(
                                        //     context, Routes.resetPasswordPage);

                                        Navigator.pushNamed(context,
                                            Routes.otpverificationPage);
                                      },
                                      child: Text(
                                        "Forgot Password?",
                                        style:
                                            mediumWhiteText14(forgotPswdColor),
                                      ),
                                    ),
                                  ),

                                  //--------------------------- sign in button -------------------------------

                                  const SizedBox(height: 30),
                                  SizedBox(
                                    height: 50,
                                    width: getScreenSize(context).width,
                                    child: TextButton(
                                        onPressed: () {
                                          loginReq();
                                        },
                                        child: Text('SIGN IN',
                                            style: boldWhiteText14(whiteColor)),
                                        style: TextButton.styleFrom(
                                            backgroundColor: primaryColor)),
                                  ),
                                  const SizedBox(height: 30),
                                ],
                              ),
                            ),
                          ),
                        ),
                      )),
                ),

                //--------------------------- black area at bottom -------------------------------------

                Expanded(
                  flex: 1,
                  child: Container(
                    child: Column(
                      children: [
                        //--------------------------- space -----------------------------------------------
                        const SizedBox(height: 20),
                        //--------------------------- dont have account text ------------------------------
                        Align(
                          alignment: Alignment.center,
                          child: Text(
                            "Don't have an account?",
                            style: regularWhiteText16(whiteColor),
                          ),
                        ),
                        //--------------------------- space -----------------------------------------------
                        const SizedBox(height: 4),
                        //--------------------------- sign up button --------------------------------------
                        TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, Routes.signupPage);
                          },
                          child: Text('SIGN UP',
                              style: boldWhiteText18(primaryColor)),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            )));
  }

  loginReq() async {
    if (await connectivityCheck()) {
      if (!isNotEmpty(email)) {
        showBanner("Enter your Email", context);
      }

      if (!isNotEmpty(password)) {
        showBanner("Enter your Phone", context);
      }

      Webservices.shared.login(email, password, context).then((value) {
        if (value != null) {
          print('${value.email}');
        }
      });
    }
  }
}
