// TODO Implement this library.

import 'package:flutter/material.dart';

import '../../utils/colors.dart';
import '../../utils/decorations.dart';
import '../../utils/styles.dart';
import '../../utils/utils.dart';
import '../../utils/widgets/textFields.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  State createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPasswordPage> {
  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // ignore: todo
    // TODO: implement build

    return Scaffold(
      backgroundColor: whiteColor,
      body: SafeArea(
        child: SingleChildScrollView(
          physics: const ClampingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                //--------------------------- back button -----------------------------------

                Container(
                  child: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),

                //--------------------------- image logo ----------------------------------
                Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/forgotPassword.png',
                    height: getScreenSize(context).width * 0.65,
                    width: getScreenSize(context).width * 0.65,
                    fit: BoxFit.fitWidth,
                  ),
                ),

                //--------------------------- headin 1 ----------------------------------

                const SizedBox(
                  height: 30,
                ),

                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Forgot Password?",
                    style: boldWhiteText42(blackTextColor),
                  ),
                ),

                //--------------------------- headin 2 ----------------------------------

                const SizedBox(height: 20),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(
                      "Enter your registered email will send you link for reset password",
                      style: regularWhiteText18(blackTextColor),
                    ),
                  ),
                ),

                //--------------------------- Email textfiled ----------------------------------

                const SizedBox(height: 40),

                CustomizedTextField(
                  name: "Email",
                  hintText: "Enter Your Email",
                  keyBoardType: TextInputType.emailAddress,
                  textChanged: (text) {},
                ),

                //--------------------------- Send Button ----------------------------------

                const SizedBox(height: 30),
                SizedBox(
                  height: 50,
                  width: getScreenSize(context).width,
                  child: TextButton(
                      onPressed: () {},
                      child: Text('SEND', style: boldWhiteText14(whiteColor)),
                      style:
                          TextButton.styleFrom(backgroundColor: primaryColor)),
                ),
                const SizedBox(height: 30),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
