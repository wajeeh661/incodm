// TODO Implement this library.

import 'package:flutter/material.dart';

import '../../utils/colors.dart';
import '../../utils/styles.dart';
import '../../utils/utils.dart';
import '../../utils/widgets/textFields.dart';
import 'package:flutter_pin_code_widget/flutter_pin_code_widget.dart';

class OTPverificationPage extends StatefulWidget {
  const OTPverificationPage({Key? key}) : super(key: key);

  @override
  State createState() => _OTPState();
}

class _OTPState extends State<OTPverificationPage> {
  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // ignore: todo
    // TODO: implement build

    var controller1 = TextEditingController();
    var controller2 = TextEditingController();
    var controller3 = TextEditingController();
    var controller4 = TextEditingController();

    return Scaffold(
      backgroundColor: whiteColor,
      body: SafeArea(
        child: SingleChildScrollView(
          physics: const ClampingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                //--------------------------- back button -----------------------------------

                Container(
                  child: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),

                //--------------------------- image logo ----------------------------------
                Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/otpVerification.png',
                    height: getScreenSize(context).width * 0.65,
                    width: getScreenSize(context).width * 0.65,
                    fit: BoxFit.fitWidth,
                  ),
                ),

                //--------------------------- headin 1 ----------------------------------

                const SizedBox(
                  height: 30,
                ),

                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Verification Code",
                    style: boldWhiteText42(blackTextColor),
                  ),
                ),

                //--------------------------- headin 2 ----------------------------------

                const SizedBox(height: 20),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(
                      "We have to sent the code for verification",
                      style: regularWhiteText18(blackTextColor),
                    ),
                  ),
                ),

                //--------------------------- Email textfiled ----------------------------------

                const SizedBox(height: 40),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    OTPDigitTextFieldBox(
                        first: true, last: false, controller: controller1),
                    OTPDigitTextFieldBox(
                        first: false, last: false, controller: controller2),
                    OTPDigitTextFieldBox(
                        first: false, last: false, controller: controller3),
                    OTPDigitTextFieldBox(
                        first: false, last: true, controller: controller4),
                  ],
                ),

                //--------------------------- Send Button ----------------------------------

                const SizedBox(height: 30),
                SizedBox(
                  height: 50,
                  width: getScreenSize(context).width,
                  child: TextButton(
                      onPressed: () {
                        print("focus: $controller1");
                      },
                      child: Text('DONE', style: boldWhiteText14(whiteColor)),
                      style:
                          TextButton.styleFrom(backgroundColor: primaryColor)),
                ),
                const SizedBox(height: 30),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _otpTextField(
      BuildContext context, bool autoFocus, FocusNode focusNode) {
    TextEditingController _text = TextEditingController(text: "");
    return Container(
      height: MediaQuery.of(context).size.shortestSide * 0.13,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
        shape: BoxShape.rectangle,
      ),
      child: AspectRatio(
        aspectRatio: 1,
        child: FocusScope(
          child: Focus(
            onFocusChange: (focus) {
              print("focus: $focus");
              _text.text = "";
            },
            child: TextField(
              focusNode: focusNode,
              controller: _text,
              autofocus: autoFocus,
              decoration: const InputDecoration(
                border: InputBorder.none,
              ),
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              style: const TextStyle(),
              maxLines: 1,
              onChanged: (value) {
                if (value.length == 1) {
                  FocusScope.of(context).nextFocus();
                }
              },
              onTap: () {
                _text.text = "";
              },
            ),
          ),
        ),
      ),
    );
  }
}
