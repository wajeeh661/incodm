// TODO Implement this library.

import 'package:flutter/material.dart';

import '../../utils/colors.dart';
import '../../utils/decorations.dart';
import '../../utils/styles.dart';
import '../../utils/utils.dart';
import '../../utils/widgets/textFields.dart';

class SignupPage extends StatefulWidget {
  const SignupPage({Key? key}) : super(key: key);

  @override
  State createState() => _SignupState();
}

class _SignupState extends State<SignupPage> {



String firstName = "";
String last_name = "";
String email = "";
String telephone = "";
String password = "";
String confirmed = "";
String company_name = "";
String company_type = "";






  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // ignore: todo
    // TODO: implement build

    return Scaffold(
      body: Container(
        height: getScreenSize(context).height,
        color: blackBackoundColor,
        child: SingleChildScrollView(
          physics: const ClampingScrollPhysics(),
          child: Column(
            children: [
              Container(
                  decoration: boxDecorationBottomRounded(whiteColor, 20),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          //--------------------------- spcae -----------------------------------

                          const SizedBox(height: 50),

                          //--------------------------- heading ----------------------------------

                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Sign Up",
                              style: boldWhiteText34(blackTextColor),
                            ),
                          ),

                          //--------------------------- space -----------------------------------

                          const SizedBox(height: 16),

                          //--------------------------- sign in heading --------------------------

                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Sign up to manage your account",
                              style: regularWhiteText18(blackTextColor),
                            ),
                          ),

                          //--------------------------- First name -----------------------------------

                          const SizedBox(height: 30),

                          CustomizedTextField(
                            name: "First Name",
                            hintText: "Enter First Name",
                            keyBoardType: TextInputType.emailAddress,
                            textChanged: (text) {
                              firstName = text;
                            },
                          ),

                          //--------------------------- Last name -----------------------------------

                          const SizedBox(height: 20),

                          CustomizedTextField(
                              name: "Last Name",
                              hintText: "Enter Last Name",
                              keyBoardType: TextInputType.emailAddress,
                              textChanged: (text) {
                                 last_name = text;
                              }),

                          //--------------------------- Phone -------------------------

                          const SizedBox(height: 20),

                          CustomizedTextField_Phone(
                            name: "Phone",
                            hintText: "Enter Your Phone",
                            textChanged: (text) {
                               telephone = text;

                            },
                          ),

                          const SizedBox(height: 20),

                          CustomizedTextField(
                              name: "Email",
                              hintText: "Enter Your Email",
                              keyBoardType: TextInputType.emailAddress,
                              textChanged: (text) {

                                email = text;
                              }),

                          //--------------------------- Country Name -----------------------

                          const SizedBox(height: 20),

                          // CustomizedTextField(
                          //     name: "Country Name",
                          //     hintText: "Enter Your Country",
                          //     keyBoardType: TextInputType.emailAddress,
                          //     textChanged: (text) {}),

//--------------------------- Company Type  -----------------------

                          const SizedBox(height: 20),

                          CustomizedTextField_DropDown(
                            name: "Company Type",
                            hintText: "Select Company Type",
                          ),

                          //--------------------------- Text field password -----------------------

                          const SizedBox(height: 20),

                          CustomizedTextField(
                              name: "Password",
                              hintText: "Enter Your Password",
                              keyBoardType: TextInputType.visiblePassword,
                              textChanged: (text) {
                                password = text;
                              }),

                          const SizedBox(height: 20),

                          CustomizedTextField(
                              name: "Confirm Password",
                              hintText: "Enter Your Password",
                              keyBoardType: TextInputType.visiblePassword,
                              textChanged: (text) {
                                confirmed = text;
                              }),

                          //--------------------------- sign in button -------------------------------

                          const SizedBox(height: 30),
                          SizedBox(
                            height: 50,
                            width: getScreenSize(context).width,
                            child: TextButton(
                                onPressed: () {

                                  signInReq();

                                },
                                child: Text('SIGN UP',
                                    style: boldWhiteText14(whiteColor)),
                                style: TextButton.styleFrom(
                                    backgroundColor: primaryColor)),
                          ),
                          const SizedBox(height: 30),
                        ],
                      ),
                    ),
                  )),
              SizedBox(
                height: 125,
                child: Column(
                  children: [
                    //--------------------------- space -----------------------------------------------
                    const SizedBox(height: 20),
                    //--------------------------- dont have account text ------------------------------
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        "Already have an account?",
                        style: regularWhiteText16(whiteColor),
                      ),
                    ),
                    //--------------------------- space -----------------------------------------------
                    const SizedBox(height: 4),
                    //--------------------------- sign up button --------------------------------------
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child:
                          Text('SIGN IN', style: boldWhiteText18(primaryColor)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }



 signInReq() async {
    if (await connectivityCheck()) {
      
      
      
    //   {
    // "first_name": "wajeeh12",
    // "last_name": "hassan",
    // "email": "wajeeh2@yopmail.com",
    // "telephone": "923126071106",
    // "password": "test123",
    // "confirmed": "test123",
    // "privacy_flag": 1,
    // "polices_flag": 1,
    // "company_name": "test company",
    // "company_type": 1
}
      
      
      
      if (!isNotEmpty(email)) {
        showBanner("Enter your Email", context);
      }

      if (!isNotEmpty(password)) {
        showBanner("Enter your Phone", context);
      }

      // Webservices.shared.login(email, password, context).then((value) {
      //   if (value != null) {
      //     print('${value.email}');
      //   }
      // });


    }
  }



}
