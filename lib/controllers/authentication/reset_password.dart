// TODO Implement this library.

import 'package:flutter/material.dart';

import '../../utils/colors.dart';
import '../../utils/decorations.dart';
import '../../utils/styles.dart';
import '../../utils/utils.dart';
import '../../utils/widgets/textFields.dart';

class ResetPasswordPage extends StatefulWidget {
  const ResetPasswordPage({Key? key}) : super(key: key);

  @override
  State createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPasswordPage> {
  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // ignore: todo
    // TODO: implement build

    return Scaffold(
      backgroundColor: whiteColor,
      body: SafeArea(
        child: SingleChildScrollView(
          physics: const ClampingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                //--------------------------- Back button -----------------------------------

                Container(
                  child: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),

                //--------------------------- image logo ----------------------------------
                Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/resetPassword.png',
                    height: getScreenSize(context).width * 0.65,
                    width: getScreenSize(context).width * 0.65,
                    fit: BoxFit.fitWidth,
                  ),
                ),

                //--------------------------- Title ----------------------------------

                const SizedBox(
                  height: 30,
                ),

                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Reset Password",
                    style: boldWhiteText42(blackTextColor),
                  ),
                ),

                //--------------------------- Desciption ----------------------------------

                const SizedBox(height: 20),

                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(
                      "Enter your registered email will send you link for reset password",
                      style: regularWhiteText18(blackTextColor),
                    ),
                  ),
                ),

                //--------------------------- password Text field ----------------------------------

                const SizedBox(height: 20),

                CustomizedTextField(
                  name: "Password",
                  hintText: "Enter Your Password",
                  keyBoardType: TextInputType.visiblePassword,
                  textChanged: (text) {},
                ),

                //--------------------------- confirm password Text field ----------------------------------

                const SizedBox(height: 20),

                CustomizedTextField(
                  name: "Confirm Password",
                  hintText: "Enter Your Password",
                  keyBoardType: TextInputType.visiblePassword,
                  textChanged: (text) {},
                ),

                //--------------------------- change password button ----------------------------------

                const SizedBox(height: 30),
                SizedBox(
                  height: 50,
                  width: getScreenSize(context).width,
                  child: TextButton(
                      onPressed: () {},
                      child: Text('CHANGE PASSWORD',
                          style: boldWhiteText14(whiteColor)),
                      style:
                          TextButton.styleFrom(backgroundColor: primaryColor)),
                ),

                const SizedBox(height: 30),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
