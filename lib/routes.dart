import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:incodm/splash_page/splash_page.dart';

import 'controllers/authentication/forgot_password.dart';
import 'controllers/authentication/login_page.dart';
import 'controllers/authentication/otp_verification.dart';
import 'controllers/authentication/reset_password.dart';
import 'controllers/authentication/signup_page.dart';

class Routes {
  static const String initial = "/";
  static const String loginPage = "/loginPage";
  static const String signupPage = "/signupPage";
  static const String forgotPassword = "/ForgotPasswordPage";
  static const String resetPasswordPage = "/ResetPasswordPage";
  static const String otpverificationPage = "/OTPverificationPage";

  static Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case initial:
        return MaterialPageRoute(builder: (context) => SplashScreen());
      case loginPage:
        return MaterialPageRoute(builder: (context) => LoginPage());
      case signupPage:
        return MaterialPageRoute(builder: (context) => SignupPage());
      case forgotPassword:
        return MaterialPageRoute(builder: (context) => ForgotPasswordPage());
      case resetPasswordPage:
        return MaterialPageRoute(builder: (context) => ResetPasswordPage());
      case otpverificationPage:
        return MaterialPageRoute(builder: (context) => OTPverificationPage());
    }
  }
}
