import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:incodm/utils/colors.dart';

import '../styles.dart';

// class CutomizedTextField extends StatelessWidget {
//   String prefixImage, postfixImage, hintText, _phone = "", _password = "";
//   bool isFeildFocus, passwordVisible;
//   TextInputType? keyboard;
//   TextEditingController? descriptionController;

//   final VoidCallback callbackEyeIcon;
//   final Function(bool)? callbackFocus;

//   CutomizedTextField(
//       {Key? key,
//       this.isFeildFocus = false,
//       required this.passwordVisible,
//       required this.keyboard,
//       this.descriptionController,
//       this.prefixImage = "",
//       this.postfixImage = "",
//       this.hintText = "",
//       required this.callbackFocus,
//       required this.callbackEyeIcon})
//       : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return Container(
//       height: 61,
//       decoration: unFocusFieldDecoration,
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           Padding(
//               padding: const EdgeInsets.only(right: 12.0),
//               child: FocusScope(
//                 child: Focus(
//                   onFocusChange: callbackFocus,
//                   child: TextField(
//                     controller: descriptionController,
//                     keyboardType: keyboard,
//                     autofocus: isFeildFocus,
//                     obscureText: passwordVisible,
//                     cursorColor: Colors.black,
//                     cursorHeight: 24,
//                     decoration: InputDecoration(
//                       border: InputBorder.none,
//                       hintStyle: TextStyle(
//                           color: Colors.black.withOpacity(0.3),
//                           fontWeight: FontWeight.w400),
//                       hintText: hintText,
//                       prefixIcon: IconButton(
//                         icon: Image.asset(
//                           prefixImage,
//                           height: 23,
//                           width: 23,
//                           // color: isFeildFocus ? appBarColor : Colors.grey,
//                         ),
//                         onPressed: () {},
//                       ),
//                       suffixIcon: IconButton(
//                         icon: postfixImage == ''
//                             ? Container()
//                             : Image.asset(
//                                 !passwordVisible
//                                     ? "assets/images/ic_open_eye.png"
//                                     : "assets/images/ic_hidden_eye.png",
//                                 height: 23,
//                                 width: 23,
//                                 // color: isFeildFocus ? appBarColor : Colors.grey,
//                               ),
//                         onPressed: callbackEyeIcon,
//                       ),
//                     ),
//                     onChanged: (value) {
//                       _password = value;
//                     },
//                   ),
//                 ),
//               )),
//         ],
//       ),
//     );
//   }
// }

class CustomizedTextField extends StatelessWidget {
  String? prefixImage;
  String? postfixImage;
  String hintText;
  String name;
  bool isFeildFocus;
  TextInputType keyBoardType;
  final Function(String) textChanged;

  // final VoidCallback callbackEyeIcon;
  // final Function(bool)? callbackFocus;

  CustomizedTextField(
      {Key? key,
      required this.name,
      required this.hintText,
      required this.keyBoardType,
      this.isFeildFocus = false,
      this.prefixImage,
      this.postfixImage,
      required this.textChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isPasswordVisible;
    // ----------------------------------------------
    // This checks if the text input type is Password
    // or something else-----------------------------
    if (keyBoardType == TextInputType.visiblePassword) {
      isPasswordVisible = false;
    } else {
      isPasswordVisible = true;
    }

    return Column(
      children: [
        // --------------------- title ------------------------

        Align(
          alignment: Alignment.centerLeft,
          child: Text(name, style: regularWhiteText14(blackTextColor)),
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          decoration: BoxDecoration(
              color: boxGreyColor,
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          child: StatefulBuilder(builder: (context, StateSetter setState) {
            return TextFormField(
                // style: TextStyle(fontSize: 18),
                keyboardType: keyBoardType,
                obscureText: isPasswordVisible ? false : true,
                cursorColor: blackTextColor,
                onChanged: (text) {
                  textChanged(text);
                },
                decoration: InputDecoration(
                    // suffixIconColor: blackTextColor,
                    border: InputBorder.none,
                    filled: true,
                    fillColor: boxGreyColor,
                    hintText: hintText,
                    suffixIcon: keyBoardType == TextInputType.visiblePassword
                        ? IconButton(
                            onPressed: () {
                              setState(() {
                                isPasswordVisible = !isPasswordVisible;
                              });
                            },
                            icon: isPasswordVisible
                                ? Icon(Icons.visibility_outlined,
                                    color: iconGreyColor)
                                : Icon(Icons.visibility_off_outlined,
                                    color: iconGreyColor),
                          )
                        : null));
          }),
        ),
      ],
    );
  }
}

class CustomizedTextField_Phone extends StatelessWidget {
  String hintText;
  String name;
  bool isFeildFocus;
  final Function(String) textChanged;
  // final Function(bool)? callbackFocus;

  CustomizedTextField_Phone(
      {Key? key,
      required this.name,
      required this.hintText,
      this.isFeildFocus = false,
      required this.textChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    CountryCode selectedCountryCode;

    return Column(
      children: [
        // --------------------- title ------------------------

        Align(
          alignment: Alignment.centerLeft,
          child: Text(name, style: regularWhiteText14(blackTextColor)),
        ),
        const SizedBox(
          height: 10,
        ),

        // --------------------- text field ------------------------

        Container(
          decoration: BoxDecoration(
            // borderRadius: const BorderRadius.all(Radius.circular(10)),
            color: boxGreyColor,
          ),
          child: StatefulBuilder(builder: (context, StateSetter setState) {
            return Row(
              children: [
                Row(
                  children: [
                    Center(
                      child: CountryCodePicker(
                        builder: (countryCode) {
                          selectedCountryCode = countryCode!;

                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(children: [
                              const SizedBox(width: 4),
                              Text(
                                "${countryCode?.dialCode}",
                                style: mediumWhiteText14(blackTextColor),
                              ),
                              const SizedBox(width: 4),
                              Icon(
                                Icons.keyboard_arrow_down_rounded,
                                color: blackTextColor,
                              )
                            ]),
                          );
                        },
                        onChanged: (CountryCode countryCode) {
                          // ignore: avoid_print
                          print("New Country selected: " +
                              countryCode.toString());
                        },
                      ),
                    ),
                    // Icon(Icons.arrow_drop_down, color: iconGreyColor)
                  ],
                ),
                Container(
                  width: 1.5,
                  height: 30,
                  color: borderGreyColor,
                ),
                Expanded(
                    flex: 1,
                    child: TextFormField(
                        keyboardType: TextInputType.phone,
                        cursorColor: blackTextColor,
                        onChanged: (text) {
                          var number = text;
                          number = '${selectedCountryCode.dialCode} + $text';
                          textChanged(number);
                        },
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          filled: true,
                          fillColor: boxGreyColor,
                          hintText: hintText,
                        )))
              ],
            );
          }),
        ),
      ],
    );
  }
}

class CustomizedTextField_DropDown extends StatelessWidget {
  String hintText;
  String name;

  String dropdownvalue = 'Item 1';

  // List of items in our dropdown menu
  var items = [
    'Item 1',
    'Item 2',
    'Item 3',
    'Item 4',
    'Item 5',
  ];

  CustomizedTextField_DropDown({
    Key? key,
    required this.name,
    required this.hintText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // --------------------- title ------------------------

        Align(
          alignment: Alignment.centerLeft,
          child: Text(name, style: regularWhiteText14(blackTextColor)),
        ),
        const SizedBox(
          height: 10,
        ),

        // --------------------- text field ------------------------
        Container(
          decoration: BoxDecoration(
            //     borderRadius: const BorderRadius.all(Radius.circular(10)),
            color: boxGreyColor,
          ),
          child: StatefulBuilder(builder: (context, StateSetter setState) {
            return Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12.0),
                    child: DropdownButton(
                      underline: Container(),

                      isExpanded: true,
                      // Initial Value
                      value: dropdownvalue,

                      // Down Arrow Icon
                      icon: const Icon(Icons.keyboard_arrow_down),

                      // Array list of items
                      items: items.map((String items) {
                        return DropdownMenuItem(
                          value: items,
                          child: Text(items),
                        );
                      }).toList(),
                      // After selecting the desired option,it will
                      // change button value to selected value
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownvalue = newValue!;
                        });
                      },
                    ),
                  ),
                ),
              ],
            );
          }),
        ),
      ],
    );
  }
}

class OTPDigitTextFieldBox extends StatelessWidget {
  bool? first;
  bool? last;
  var controller = TextEditingController();

  OTPDigitTextFieldBox(
      {Key? key,
      required this.first,
      required this.last,
      required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 85,
      child: AspectRatio(
        aspectRatio: 1.0,
        child: TextField(
          autofocus: true,
          onChanged: (value) {
            if (value.length == 1 && last == false) {
              FocusScope.of(context).nextFocus();
            }
            if (value.length == 0 && first == false) {
              FocusScope.of(context).previousFocus();
            }
          },
          showCursor: false,
          readOnly: false,
          textAlign: TextAlign.center,
          style: semiBoldWhiteText16(blackTextColor),
          keyboardType: TextInputType.number,
          maxLength: 1,
          decoration: InputDecoration(
            // contentPadding: EdgeInsets.all(0),
            counter: Offstage(),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(width: 2, color: primaryColor),
                borderRadius: BorderRadius.circular(10)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(width: 2, color: forgotPswdColor),
                borderRadius: BorderRadius.circular(10)),
            //hintText: "*",
            hintStyle: semiBoldWhiteText14(blackTextColor),
          ),
          controller: controller,
          onTap: () {
            controller.text = "";
          },
        ),
      ),
    );
  }
}
