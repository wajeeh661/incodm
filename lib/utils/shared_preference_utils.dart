import 'dart:convert';

import 'package:incodm/webservices/models/authentication.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants.dart';

class SharedPreferenceKeys {
  static final String COMPANY_NAME = "COMPANY_NAME";
  static final String EMAIL = "EMAIL";
  static final String PASSWORD = "PASSWORD";
  static final String USER_MODEL = "USER_MODEL";
  static final String TOKEN = "TOKEN";
  static final String USER_LOGS = "USER_LOGS";
  static final String ALL_DATA_MODEL = "ALL_DATA_MODEL";
}

class SharedPreferenceUtils {
  static void saveUser(String user) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(SharedPreferenceKeys.USER_MODEL, user);
  }

  static Future<User?> getUser(User user) async {
    final prefs = await SharedPreferences.getInstance();
    var responseBody = prefs.getString(SharedPreferenceKeys.USER_MODEL);

    User? user;
    if (responseBody != null) {
      var jsonMap = jsonDecode(responseBody);
      user = User.fromJson(jsonMap);
    }
    return user;
  }
}
