import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:incodm/utils/colors.dart';
import 'package:incodm/utils/styles.dart';
import 'package:intl/intl.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

import '../constants.dart';

void showToast(String content) {
  Fluttertoast.showToast(
      msg: content,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black.withOpacity(0.6),
      textColor: Colors.white,
      fontSize: 16.0);
}

Future<bool> connectivityCheck() async {
  bool connect = false;

  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile) {
    // I am connected to a mobile network.
    connect = true;
    return connect;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    // I am connected to a wifi network.
    connect = true;
    return connect;
  } else {
    return connect;
  }
}

bool isNotEmpty(value) {
  if (value != null && value != '') {
    return true;
  }
  return false;
}

bool isEmpty(value) {
  if (value == null || value == '') {
    return true;
  }
  return false;
}

String showDate(String date) {
  return DateFormat(Constants.DATE_UI_FORMAT).format(DateTime.parse(date));
}

String showTime(String date) {
  return DateFormat(Constants.TIME_UI_FORMAT).format(DateTime.parse(date));
}

String formatHHMMSS(int seconds) {
  if (seconds != null && seconds != 0) {
    int hours = (seconds / 3600).truncate();
    seconds = (seconds % 3600).truncate();
    int minutes = (seconds / 60).truncate();

    String hoursStr = (hours).toString().padLeft(2, '0');
    String minutesStr = (minutes).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');

    return "${hoursStr}h:${minutesStr}m:${secondsStr}s";
  } else {
    return "00h:00m:00s";
  }
}

Widget buildTimerIndication(int? projectId, [int? employeeId]) {
  int length;
  if (isEmpty(employeeId)) {
    length = logs
        .where((element) =>
            ((element[JsonKeys.check_in][JsonKeys.projectId]) == projectId) &&
            isEmpty(element[JsonKeys.check_out]))
        .toList()
        .length;
  } else {
    length = logs
        .where((element) =>
            ((element[JsonKeys.check_in][JsonKeys.projectId]) == projectId) &&
            ((element[JsonKeys.check_in][JsonKeys.employeeId]) == employeeId) &&
            isEmpty(element[JsonKeys.check_out]))
        .toList()
        .length;
  }

  return length > 0
      ? Image.asset(
          'assets/images/ic_clock.png',
          height: 24,
        )
      : Container();
}

Size getScreenSize(BuildContext context) {
  return MediaQuery.of(context).size;
}

showSnackBar(String messgage, BuildContext context) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content: Text(messgage),
    action: SnackBarAction(
      onPressed: () {},
      label: "",
    ),
  ));
}

showBanner(String messgage, BuildContext context) {
  ScaffoldMessenger.of(context).showMaterialBanner(MaterialBanner(
      content: Center(child: Text(messgage)),
      backgroundColor: primaryColor,
      contentTextStyle: regularWhiteText16(whiteColor),
      leadingPadding: const EdgeInsets.only(right: 30),
      leading: const Icon(
        Icons.info,
        size: 32,
        color: Colors.white,
      ),
      actions: [
        TextButton(
            onPressed: () {},
            child: Text(
              'Dismiss',
              style: mediumWhiteText16(blackTextColor),
            )),
      ]));

  Future.delayed(const Duration(milliseconds: 2000), () {
    ScaffoldMessenger.of(context).hideCurrentMaterialBanner();
  });
}
