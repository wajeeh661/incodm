// ignore_for_file: unnecessary_const

import 'package:flutter/material.dart';

var appBarColor = const Color(0xff343E55);
var pinkColor = const Color(0xffFFE7DC);
var containerColor = const Color(0xffE5E5E5);
var circularBoxColor = const Color(0xff9F9F9F);
var textColor = const Color(0x4d2b3857);
var darkBtnColor = const Color(0xff858585);
var greyColor = const Color(0xff838383);
var greenColor = const Color(0xff6FC787);
var fieldColor = const Color(0xffF1F1F1);

var primaryColor = const Color(0xffE79436);
var blackBackoundColor = const Color(0xFF000000).withOpacity(0.9);
var whiteColor = const Color(0xffFFFFFF);
var blackTextColor = const Color(0xFF1F1F1F);
var boxGreyColor = const Color(0xFFF9F9F9);
var iconGreyColor = const Color(0xFF6E7077);
var forgotPswdColor = const Color(0xFF346979);
var borderGreyColor = const Color(0xFFDEDEE4);

const MaterialColor primaryColorSwatch = const MaterialColor(
  0xffE65627,
  // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
  const <int, Color>{
    50: const Color(0xffE79436), //10%
    100: const Color(0xffE79436), //20%
    200: const Color(0xffE79436), //30%
    300: const Color(0xffE79436), //40%
    400: const Color(0xffE79436), //50%
    500: const Color(0xffE79436), //60%
    600: const Color(0xffE79436), //70%
    700: const Color(0xffE79436), //80%
    800: const Color(0xffE79436), //90%
    900: const Color(0xffE79436), //100%
  },
);
