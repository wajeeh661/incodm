class generalClass {
  generalClass({
    this.message,
  });

  String? message;

  factory generalClass.fromJson(Map<String, dynamic> json) =>
      generalClass(message: json["message"]);

  Map<String, dynamic> toJson() => {"message": message};
}

class loginResp {
  bool? status;
  int? code;
  Data? data;

  loginResp({this.status, this.code, this.data});

  loginResp.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    code = json['code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data?.toJson();
    }
    return data;
  }
}

class Data {
  User? user;
  Data({this.user});

  Data.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class SignupResp {
  User? user;
  SignupResp({this.user});

  SignupResp.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class User {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  int? roleId;
  String? telephone;
  Null? countryCode;
  Null? formattedTelephone;
  Null? provider;
  Null? providerId;
  LastLogin? lastLogin;
  int? status;
  Null? telephoneVerifiedAt;
  Null? emailVerifiedAt;
  String? createdAt;
  String? updatedAt;
  List<Companies>? companies;
  String? accessToken;

  User(
      {this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.roleId,
      this.telephone,
      this.countryCode,
      this.formattedTelephone,
      this.provider,
      this.providerId,
      this.lastLogin,
      this.status,
      this.telephoneVerifiedAt,
      this.emailVerifiedAt,
      this.createdAt,
      this.updatedAt,
      this.companies,
      this.accessToken});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    roleId = json['role_id'];
    telephone = json['telephone'];
    countryCode = json['country_code'];
    formattedTelephone = json['formatted_telephone'];
    provider = json['provider'];
    providerId = json['provider_id'];
    lastLogin = json['last_login'] != null
        ? new LastLogin.fromJson(json['last_login'])
        : null;
    status = json['status'];
    telephoneVerifiedAt = json['telephone_verified_at'];
    emailVerifiedAt = json['email_verified_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['companies'] != null) {
      companies = <Companies>[];
      json['companies'].forEach((v) {
        companies!.add(new Companies.fromJson(v));
      });
    }
    accessToken = json['access_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['role_id'] = this.roleId;
    data['telephone'] = this.telephone;
    data['country_code'] = this.countryCode;
    data['formatted_telephone'] = this.formattedTelephone;
    data['provider'] = this.provider;
    data['provider_id'] = this.providerId;
    if (this.lastLogin != null) {
      data['last_login'] = this.lastLogin!.toJson();
    }
    data['status'] = this.status;
    data['telephone_verified_at'] = this.telephoneVerifiedAt;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.companies != null) {
      data['companies'] = this.companies!.map((v) => v.toJson()).toList();
    }
    data['access_token'] = this.accessToken;
    return data;
  }
}

class LastLogin {
  String? date;
  String? device;
  String? location;

  LastLogin({this.date, this.device, this.location});

  LastLogin.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    device = json['device'];
    location = json['location'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['device'] = this.device;
    data['location'] = this.location;
    return data;
  }
}

class Companies {
  int? id;
  String? name;
  Null? description;
  Null? email;
  Null? address;
  Null? logo;
  int? userId;
  int? typeId;
  String? status;
  String? createdAt;
  String? updatedAt;

  Companies(
      {this.id,
      this.name,
      this.description,
      this.email,
      this.address,
      this.logo,
      this.userId,
      this.typeId,
      this.status,
      this.createdAt,
      this.updatedAt});

  Companies.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    email = json['email'];
    address = json['address'];
    logo = json['logo'];
    userId = json['user_id'];
    typeId = json['type_id'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['email'] = this.email;
    data['address'] = this.address;
    data['logo'] = this.logo;
    data['user_id'] = this.userId;
    data['type_id'] = this.typeId;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
