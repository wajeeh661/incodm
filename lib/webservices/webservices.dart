import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:incodm/utils/utils.dart';
import 'package:incodm/webservices/models/authentication.dart';

import '../constants.dart';
import 'package:http/http.dart' as http;

import '../utils/shared_preference_utils.dart';

class Webservices {
  static final shared = Webservices();

  Future<User?> login(
      String email, String password, BuildContext context) async {
    // String url = '${Constants.BASE_URL}/account/signup';
    String url = '${Constants.BASE_URL}/account/login';

    Map<String, String> params = {
      "email": email,
      "password": password,
    };

    Map<String, String> headers = {
      "Content-Type": "application/json",
    };

    User? userModel = null;

    var jsonBody = jsonEncode(params);

    print('--> url --> $url');
    print('--> header --> ${params.toString()}');
    print('--> params --> ${headers.toString()}');

    var response = await http.post(Uri.parse(url),
        headers: headers,
        body: jsonBody,
        encoding: Encoding.getByName("utf-8"));

    print('--> statusCode --> ${response.statusCode}');

    var responseBody = response.body;
    var jsonMap = jsonDecode(responseBody);

    print('--> response --> ${responseBody}');

    if (response.statusCode == 200 || response.statusCode == 201) {
      userModel = loginResp.fromJson(jsonMap).data?.user;
      SharedPreferenceUtils.saveUser(responseBody);
    } else if (response.statusCode == 401) {
      var resp = generalClass.fromJson(jsonMap);
      showBanner(resp.message ?? "", context);
    }

    print('--> response --> ${userModel.toString()}');

    return userModel;
  }

  Future<User?> signup(Map<String, String> params, BuildContext context) async {
    String url = '${Constants.BASE_URL}/account/signup';

    // Map<String, String> params = {
    //   "email": email,
    //   "password": password,
    // };

    Map<String, String> headers = {
      "Content-Type": "application/json",
    };

    User? userModel = null;

    var jsonBody = jsonEncode(params);

    print('--> url --> $url');
    print('--> header --> ${params.toString()}');
    print('--> params --> ${headers.toString()}');

    var response = await http.post(Uri.parse(url),
        headers: headers,
        body: jsonBody,
        encoding: Encoding.getByName("utf-8"));
    if (response.statusCode == 200 || response.statusCode == 201) {
      var responseBody = response.body;
      var jsonMap = jsonDecode(responseBody);
      userModel = SignupResp.fromJson(jsonMap).user;
      SharedPreferenceUtils.saveUser(responseBody);
    } else {}

    print('--> response --> ${userModel.toString()}');

    return userModel;
  }
}
