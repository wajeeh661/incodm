import 'package:flutter/material.dart';
import 'package:incodm/routes.dart';
import 'package:incodm/utils/colors.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Construction',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Inter',
        primarySwatch: primaryColorSwatch,
      ),
      initialRoute: Routes.initial,
      onGenerateRoute: Routes.onGenerateRoute,
    );
  }
}
