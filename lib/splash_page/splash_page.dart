import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

import '../routes.dart';
import '../utils/utils.dart';
import '../utils/widgets/widgets.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;
  bool globalUserModel = false;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(seconds: 3),
      vsync: this,
    );

    _animation = Tween(begin: 0.0, end: 1.0).animate(_controller);
    _controller.forward(from: 0.0);

    _controller.addListener(() {
      if (_animation.isCompleted) {
        if (globalUserModel) {
          print('go to home');
          //   Navigator.of(context).pushReplacementNamed('/homePage');
        } else {
          print('go to login');
          Navigator.of(context).pushReplacementNamed(Routes.loginPage);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      body: FadeTransition(
        opacity: _animation,
        child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
              'assets/images/backround.jpeg',
            )),
          ),
          child: Center(
            child: Container(
              width: getScreenSize(context).width * 0.68,
              child: Image.asset(
                'assets/images/logo.jpg',
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
